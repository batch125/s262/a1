//users with A in firstName OR lastName

db.users.find(
        {
        $or: [
                {"firstName":{$regex:"A",$options:"i"}},
                {"lastName":{$regex:"A",$options:"i"}}
                ]
        },
        {
        	"_id": 0,
        }
        );

//users who are admin and active
db.users.find(
		{
			$and: [
				{"isAdmin": true},
				{"isActive":true}
				]
		}
		);

//courses with "u" in the name and price of greater than or equal to 13000
db.course.find(
        {
        $and: [
                {"name":{$regex:"u",$options:"i"}},
                {"Price":{$gte:"13000"}}
                ]       
        }
        );